package com.yukon.ezra.task1.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Ezra on 11/22/2017.
 */
@RestController
public class Task1RestController {

    @RequestMapping("/lol")
    public String lol()
    {
        return "kreosmms;scmsam";
    }

}
