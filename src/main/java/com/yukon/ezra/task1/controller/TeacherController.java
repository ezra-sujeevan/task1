package com.yukon.ezra.task1.controller;

import com.yukon.ezra.task1.model.Teacher;
import com.yukon.ezra.task1.repository.StudentRepository;
import com.yukon.ezra.task1.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

/**
 * Created by Ezra on 11/23/2017.
 */
@Controller
@RequestMapping(path = "/teacher")
public class TeacherController {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public String viewTeachers(Model model) {
        model.addAttribute("teachers", teacherRepository.findAll());
        return "teacher/view";
    }

    @RequestMapping(path = {"/edit"}, method = RequestMethod.GET)
    public String getTeacher(Model model, @RequestParam(name = "id", required = false, defaultValue = "0") Long id) {
        Teacher teacher;
        if (id == 0) {
            teacher = new Teacher();
        } else if (teacherRepository.exists(id)) {
            teacher = teacherRepository.getOne(id);
        } else {
            return null;
        }
        model.addAttribute("students", studentRepository.findAll());
        model.addAttribute("teacher", teacher);
        return "teacher/edit";
    }

    @RequestMapping(path = {"/edit"}, method = RequestMethod.POST)
    public String saveTeacher(Model model, @Valid @ModelAttribute(name = "teacher", value = "teacher") Teacher teacher, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "teacher/edit";
        }
        teacherRepository.save(teacher);
        return "redirect:/";
    }

    @RequestMapping(path = {"/delete"}, method = RequestMethod.DELETE)
    public String saveTeacher(Model model, @RequestParam(name = "id") Long id) {

        if (teacherRepository.exists(id)) {
            teacherRepository.delete(id);
        }
        return "redirect:/";
    }
}

