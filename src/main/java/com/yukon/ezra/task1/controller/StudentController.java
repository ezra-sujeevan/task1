package com.yukon.ezra.task1.controller;

import com.yukon.ezra.task1.model.Student;
import com.yukon.ezra.task1.repository.StudentRepository;
import com.yukon.ezra.task1.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Ezra on 11/23/2017.
 */
@Controller
@RequestMapping(path = "/student")
public class StudentController {


    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private StudentRepository studentRepository;


    @RequestMapping(path = "", method = RequestMethod.GET)
    public String studentView(Model model) {
        model.addAttribute("students", studentRepository.findAll());
        return "student/view";
    }

    @RequestMapping(path = {"/edit"}, method = RequestMethod.GET)
    public String getStudent(Model model, @RequestParam(name = "id", required = false, defaultValue = "0") Long id) {
        Student student;
        if (id == 0) {
            student = new Student();
        } else if (studentRepository.exists(id)) {
            student = studentRepository.getOne(id);
        } else {
            return null;
        }
        model.addAttribute("teachers", teacherRepository.findAll());
        model.addAttribute("student", student);
        return "student/edit";
    }

    @RequestMapping(path = {"/edit"}, method = RequestMethod.POST)
    public String saveStudent(Model model, @Validated @ModelAttribute(name = "student", value = "student") Student student, BindingResult bindingResult) {
        System.out.println(model.asMap());

        if (bindingResult.hasErrors()) {
            return "student/edit";
        }
        studentRepository.save(student);
        return "redirect:/";
    }

    @RequestMapping(path = {"/delete"}, method = RequestMethod.DELETE)
    public String saveStudent(Model model, @RequestParam(name = "id") Long id) {

        if (studentRepository.exists(id)) {
            studentRepository.delete(id);
        }
        return "redirect:/";
    }
}
