package com.yukon.ezra.task1.repository;

import com.yukon.ezra.task1.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

/**
 * Created by Ezra on 11/21/2017.
 */

public interface StudentRepository extends JpaRepository<Student, Long> {



}
