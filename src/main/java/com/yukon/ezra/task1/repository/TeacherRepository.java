package com.yukon.ezra.task1.repository;

import com.yukon.ezra.task1.model.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Ezra on 11/23/2017.
 */
public interface TeacherRepository extends JpaRepository<Teacher,Long> {
}
