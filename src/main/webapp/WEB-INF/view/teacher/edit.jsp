<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Log in with your account</title>

    <link href="${contextPath}/resources/bootstrap-4.0.0-beta.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light">
        <span class="navbar-brand mb-0 h1">Ezra</span>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item ">
                    <a class="nav-link" href="<spring:url value="/"/>">Home</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="<spring:url value="/student"/>">Student </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="<spring:url value="/teacher"/>">Teacher
                        <span class="sr-only">(current)</span></a>
                </li>
                <%--<li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">Disabled</a>
                </li>--%>
            </ul>
        </div>
    </nav>
    <div class="row">
        <div class="col text-center">
            <h1>Teacher Management Page</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <form:form action="${pageContext.request.contextPath}/teacher/edit/" method="post" commandName="teacher"
                       id="teacher" modelAttribute="teacher">

                <form:input path="id" type="hidden"/>

                <div class="form-group row">
                    <spring:bind path="first_name">
                        <label for="first_name">First Name</label>
                        <form:input path="first_name" type="text"
                                    class="form-control ${status.error ? 'is-invalid' : ''}"
                                    aria-describedby="first_name"
                                    placeholder="First Name"/>
                        <small id="first_name" class="form-text text-muted">Minimum No. of characters 5</small>
                        <div class="invalid-feedback"> ${status.errorMessage} </div>
                    </spring:bind>
                </div>

                <div class="form-group row">
                    <spring:bind path="last_name">
                        <label for="last_name">Last Name</label>
                        <form:input path="last_name" type="text"
                                    class="form-control ${status.error ? 'is-invalid' : ''}"
                                    aria-describedby="last_name"
                                    placeholder="Last Name"/>

                        <small id="last_name" class="form-text text-muted">Minimum No. of characters 5</small>
                        <div class="invalid-feedback"> ${status.errorMessage} </div>
                    </spring:bind>
                </div>

                <div class="form-group row">
                    <spring:bind path="dob">
                        <label for="dob">Date of Birth</label>
                        <form:input path="dob" type="date"
                                    class="form-control ${status.error ? 'is-invalid' : ''}"
                                    aria-describedby="dob"
                                    placeholder="Date of Birth"/>
                        <small id="dob" class="form-text text-muted">Minimum No. of characters 5</small>
                        <div class="invalid-feedback"> ${status.errorMessage} </div>
                    </spring:bind>
                </div>

                <div class="form-group row">
                    <spring:bind path="email">
                        <label for="email">E-mail</label>
                        <form:input path="email" type="text"
                                    class="form-control ${status.error ? 'is-invalid' : ''}"
                                    aria-describedby="email"
                                    placeholder="E-Mail"/>
                        <small id="email" class="form-text text-muted">Minimum No. of characters 5</small>
                        <div class="invalid-feedback"> ${status.errorMessage} </div>
                    </spring:bind>
                </div>
                <div class="form-group">
                    <label for="students">Teachers</label>
                    <form:select path="students" multiple="multiple" class="form-control">
                        <form:options items="${students}" itemValue="id" itemLabel="first_name"/>
                    </form:select>
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form:form>
        </div>
    </div>

</div>

</body>
<script src="${contextPath}/resources/jquery-3.2.1/jquery-3.2.1.min.js"></script>
<script src="${contextPath}/resources/bootstrap-4.0.0-beta.2/js/bootstrap.bundle.js"></script>
</html>